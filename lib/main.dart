import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:toast/toast.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
    // This widget is the root of your application.
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
                primarySwatch: Colors.blue,
            ),
            home: MyHomePage(title: 'Flutter Demo Home Page'),
        );
    }
}

class MyHomePage extends StatefulWidget {
    MyHomePage({Key key, this.title}) : super(key: key);

    final String title;

    @override
    _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
    int _counter = 0;
    MqttClient client = MqttClient('test.mosquitto.org', '');
    bool switch1 = false;
    var _payloadMessage = '';


    _MyHomePageState() {

    }

    @override
    void initState() {
        super.initState();
        this.setMqttClient();
    }

    setMqttClient() async {

        /// If you intend to use a keep alive value in your connect message that is not the default(60s)
        /// you must set it here
        client.keepAlivePeriod = 36000;

        /// Add the successful connection callback
        client.onConnected = () {
            print('좁속24234324324 성공하였씁니다sdlfksldkflsdkf');
        };

        client.onDisconnected = () {
            print('EXAMPLE::OnDisconnected client callback - Client disconnection');
            if (client.connectionStatus.returnCode == MqttConnectReturnCode.solicited) {
                print('EXAMPLE::OnDisconnected callback is solicited, this is correct');
            }
        };


        client.onSubscribed = (String topic) {
            print('EXAMPLE::Subscription confirmed for topic $topic');
        };


        /// Create a connection message to use or use the default one. The default one sets the
        /// client identifier, any supplied username/password, the default keepalive interval(60s)
        /// and clean session, an example of a specific one below.
        final MqttConnectMessage connMess = MqttConnectMessage()
            .withClientIdentifier('Mqtt_MyClientUniqueId')
            .keepAliveFor(3600) // Must agree with the keep alive set above or not set
            .withWillTopic('willtopic') // If you set this you must set a will message
            .withWillMessage('My Will message')
            .startClean() // Non persistent session for testing
            .withWillQos(MqttQos.atLeastOnce);
        print('EXAMPLE::Mosquitto client connecting....');
        client.connectionMessage = connMess;

        /// Connect the client, any errors here are communicated by raising of the appropriate exception. Note
        /// in some circumstances the broker will just disconnect us, see the spec about this, we however eill
        /// never send malformed messages.
        try {
            await client.connect();
        } on Exception catch (e) {
            print('접속 실패하였습니다.sdlfksdlkf - $e');
            client.disconnect();
        }

        /// Check we are connected
        if (client.connectionStatus.state == MqttConnectionState.connected) {
            print('연결되어있어요ㅕ!!!!!!!!!!!!!!!!!');
        } else {
            /// Use status here rather than state if you also want the broker return code.
            print('연결 안되어 이써요... ${client.connectionStatus}');
            client.disconnect();
            exit(-1);
        }
        /* client.updates.listen((List<MqttReceivedMessage<MqttMessage>> cueList) {
            final MqttPublishMessage recMess = cueList[0].payload;
            final String payloadMessage = MqttPublishPayload.bytesToStringAsString(recMess.payload.message);
            print('==============>>>>${cueList[0].topic}');

            print('paylaodMessage==>' + payloadMessage);
            setState(() {
                _payloadMessage = payloadMessage;
            });
        });*/

        //todo: ##########################
        //todo: 메세지 리스너 입니다         #
        //todo: ##########################
        client.published.listen((MqttPublishMessage message) {
            //print('zzzzzzzzz====>>> ${message.variableHeader.topicName}, with Qos ${message.header.qos} ====> ${message.payload.toString()}');

            final String payloadMessage = MqttPublishPayload.bytesToStringAsString(message.payload.message);

            print('message listen payload message===>$payloadMessage');

            setState(() {
                _payloadMessage = payloadMessage;
                payloadMessage == "on" ? this.switch1 = true : this.switch1 = false;
            });

            Toast.show('mqtt message listener===>' + payloadMessage.toString(), context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        });

        final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
        builder.addString('고경준 천재님이십니다sdlfksdlkflsdkflklkdsflk');


        /// Publish it
        print('EXAMPLE::Publishing our topic');

        client.publishMessage('kyungjoon', MqttQos.atLeastOnce, builder.payload);


        client.subscribe('kyungjoon', MqttQos.atLeastOnce);
    }

    void _incrementCounter() {
        setState(() {
            _counter++;
        });
    }

    bool _value1 = false;

    //todo :###################
    //todo : 스위치 체인지 EV
    //todo :###################
    _onSwitchChange(bool changedValue) {
        if (changedValue == true) {
            final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
            builder.addString('on');
            client.publishMessage('kyungjoon', MqttQos.atLeastOnce, builder.payload);
        } else {
            final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
            builder.addString('off');
            client.publishMessage('kyungjoon', MqttQos.atLeastOnce, builder.payload);
        }
        setState(() {
            switch1 = changedValue;
        });
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                backgroundColor: Colors.green,
                title: Text('고경준천재님잇비sdlfksdlkflsdkf'),
            ),
            body: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                        Text(
                            '고경준 천재sdlkflsdkfasldlaskdlk',
                        ),
                        Text(
                            '$_counter',
                            style: Theme
                                .of(context)
                                .textTheme
                                .display1,
                        ),
                        RaisedButton(
                            color: Colors.blue,
                            child: Text('sdlfkslfk'),
                            onPressed: () {
                                print('slfksdlkf');
                            },
                        ),
                        RaisedButton(
                            color: Colors.blue,
                            child: Text('sdlfkslfk'),
                            onPressed: () {
                                print('slfksdlkf');
                            },
                        ),
                        RaisedButton(
                            color: Colors.blue,
                            child: Text('sdlfkslfk'),
                            onPressed: () {
                                print('slfksdlkf');
                            },
                        ),

                        RaisedButton(
                            color: Colors.green,
                            child: Text('sdlfkslfk'),
                            onPressed: () {
                                print('slfksdlkf');
                            },
                        ),
                        RaisedButton(
                            color: Colors.blue,
                            child: Text('sdlfkslfk'),
                            onPressed: () {
                                print('slfksdlkf');
                            },
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[

                                Container(
                                    child: RaisedButton(
                                        color: Colors.red,
                                        child: Text('publishMessage_on'),
                                        onPressed: () {
                                            final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
                                            builder.addString('on');
                                            client.publishMessage('kyungjoon', MqttQos.atLeastOnce, builder.payload);
                                            setState(() {
                                                switch1 = true;
                                            });
                                        },
                                    ),
                                    width: 100,

                                ),
                                Container(
                                    width: 20,
                                ),
                                Container(
                                    width: 100,
                                    child: RaisedButton(
                                        color: Colors.blue,
                                        child: Text('publishMessage_off'),
                                        onPressed: () {
                                            final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
                                            builder.addString('off');
                                            client.publishMessage('kyungjoon', MqttQos.atLeastOnce, builder.payload);

                                            setState(() {
                                                switch1 = false;
                                            });
                                        },
                                    ),
                                ),
                                Container(
                                    width: 20,
                                ),

                                Container(
                                    child: RaisedButton(
                                        color: Colors.red,
                                        child: Text('publishMessage_on'),
                                        onPressed: () {
                                            final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
                                            builder.addString('on');
                                            client.publishMessage('kyungjoon', MqttQos.atLeastOnce, builder.payload);
                                            setState(() {
                                                switch1 = true;
                                            });
                                        },
                                    ),
                                    width: 100,
                                ),

                                Container(
                                    width: 5,
                                ),

                            ],
                        ),
                        Container(
                            child: Text(this._payloadMessage + "111111111"),
                        ),
                        CupertinoSwitch(value: switch1, activeColor: Colors.purple, onChanged: this._onSwitchChange),
                        CupertinoButton.filled(
                            borderRadius: BorderRadius.circular(50.0),
                            onPressed: () {
                                print('sdflksdflksdf');
                            },
                            child: Text('123213123'),
                        ),
                        CupertinoButton(
                            onPressed: () {
                                print('sdflksdflksdf');
                            },
                            child: Text('123213123'),
                        )


                    ],
                ),
            ),
            floatingActionButton: FloatingActionButton(
                onPressed: _incrementCounter,
                tooltip: 'Increment',
                child: Icon(Icons.star),
            ), // This trailing comma makes auto-formatting nicer for build methods.
        );
    }
}
