import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:mqtt_client/mqtt_client.dart';


final MqttClient client = MqttClient('test.mosquitto.org', '');

Future<int> main() async {
    client.logging(on: false);

    /// If you intend to use a keep alive value in your connect message that is not the default(60s)
    /// you must set it here
    client.keepAlivePeriod = 20;

    /// Add the successful connection callback
    client.onConnected = () {
        print('좁속24234324324 성공하였씁니다sdlfksldkflsdkf');
    };

    client.onDisconnected = () {
        print('EXAMPLE::OnDisconnected client callback - Client disconnection');
        if (client.connectionStatus.returnCode == MqttConnectReturnCode.solicited) {
            print('EXAMPLE::OnDisconnected callback is solicited, this is correct');
        }
        exit(-1);
    };

    /// Add a subscribed callback, there is also an unsubscribed callback if you need it.
    /// You can add these before connection or change them dynamically after connection if
    /// you wish. There is also an onSubscribeFail callback for failed subscriptions, these
    /// can fail either because you have tried to subscribe to an invalid topic or the broker
    /// rejects the subscribe request.
    client.onSubscribed = (String topic) {
        print('EXAMPLE::Subscription confirmed for topic $topic');
    };

    /// Set a ping received callback if needed, called whenever a ping response(pong) is received
    /// from the broker.
    client.pongCallback = () {
        print('EXAMPLE::Ping response client callback invoked');
    };

    /// Create a connection message to use or use the default one. The default one sets the
    /// client identifier, any supplied username/password, the default keepalive interval(60s)
    /// and clean session, an example of a specific one below.
    final MqttConnectMessage connMess = MqttConnectMessage()
        .withClientIdentifier('Mqtt_MyClientUniqueId')
        .keepAliveFor(20) // Must agree with the keep alive set above or not set
        .withWillTopic('willtopic') // If you set this you must set a will message
        .withWillMessage('My Will message')
        .startClean() // Non persistent session for testing
        .withWillQos(MqttQos.atLeastOnce);
    print('EXAMPLE::Mosquitto client connecting....');
    client.connectionMessage = connMess;

    /// Connect the client, any errors here are communicated by raising of the appropriate exception. Note
    /// in some circumstances the broker will just disconnect us, see the spec about this, we however eill
    /// never send malformed messages.
    try {
        await client.connect();
    } on Exception catch (e) {
        print('EXAMPLE::client exception - $e');
        client.disconnect();
    }

    /// Check we are connected
    if (client.connectionStatus.state == MqttConnectionState.connected) {
        print('EXAMPLE::Mosquitto client connected');
    } else {
        /// Use status here rather than state if you also want the broker return code.
        print(
            'EXAMPLE::ERROR Mosquitto client connection failed - disconnecting, status is ${client.connectionStatus}');
        client.disconnect();
        exit(-1);
    }

    /// Ok, lets try a subscription
    print('EXAMPLE::Subscribing to the test/lol topic');
    const String topic = 'test/lol'; // Not a wildcard topic
    client.subscribe(topic, MqttQos.atMostOnce);

    /// The client has a change notifier object(see the Observable class) which we then listen to to get
    /// notifications of published updates to each subscribed topic.
    client.updates.listen((List<MqttReceivedMessage<MqttMessage>> c) {
        final MqttPublishMessage recMess = c[0].payload;
        final String payloadMessage = MqttPublishPayload.bytesToStringAsString(recMess.payload.message);
        print('==============>>>>${c[0].topic}');

        print('paylaodMessage==>' + payloadMessage);
    });

    /// If needed you can listen for published messages that have completed the publishing
    /// handshake which is Qos dependant. Any message received on this stream has completed its
    /// publishing handshake with the broker.
    client.published.listen((MqttPublishMessage message) {
        print('EXAMPLE::Published notification:: topic is ${message.variableHeader.topicName}, with Qos ${message.header.qos}');
    });

    const String pubTopic = 'Dart/Mqtt_client/testtopic';
    final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
    builder.addString('고경준 천재님이십니다sdlfksdlkflsdkflklkdsflk');


    /// Publish it
    print('EXAMPLE::Publishing our topic');

    var timer = new Timer(const Duration(seconds: 3), () {
        client.publishMessage('kyungjoon', MqttQos.atLeastOnce, builder.payload);
    });


    client.subscribe('kyungjoon', MqttQos.atLeastOnce);


    return 0;
}

